<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DailyRecord extends Model
{
    use HasFactory;

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'uuid'; // Define your UUID column as primary key

    public static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->uuid = Str::uuid();

            // Hitung rata-rata usia pria
            $maleAvgAge = User::where('gender', 'male')->avg('age');

            // Hitung rata-rata usia wanita
            $femaleAvgAge = User::where('gender', 'female')->avg('age');

            // Simpan hasil perhitungan ke dalam model DailyRecord
            $model->male_avg_age = $maleAvgAge;
            $model->female_avg_age = $femaleAvgAge;
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'date',
        'male_count',
        'female_count',
        'male_avg_age',
        'female_avg_age'
    ];

    /* Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public $incrementing = false;

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    protected $keyType = 'string';
}
