<?php

namespace App\Listeners;

use App\Events\UserDeleted;
use App\Jobs\RecalculateDailyRecords;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateDailyRecords
{
    /**
     * Create the event listener.
     *
     * Constructor method.
     */
    public function __construct()
    {
        // Konstruktor kosong karena tidak ada dependensi yang perlu diinjeksikan.
    }

    /**
     * Handle the event.
     *
     * Menghandle event ketika pengguna dihapus.
     *
     * @param UserDeleted $event
     */
    public function handle(UserDeleted $event): void
    {
        // Mengambil informasi pengguna dari event
        $user = $event->user;
        // Mengambil jenis kelamin pengguna yang dihapus
        $userGender = $event->user->gender;
        // Menghapus pengguna dari database
        $user->delete();

        // Mengirim pekerjaan untuk menghitung ulang rekaman harian pengguna
        RecalculateDailyRecords::dispatch($userGender);
    }
}
