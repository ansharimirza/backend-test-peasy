<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\DailyRecordService;
use App\Http\Traits\ApiResponder;

class DailyRecordController extends Controller
{
    use ApiResponder; // Menggunakan trait ApiResponder untuk respons API

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Services\DailyRecordService  $DailyRecordService
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, DailyRecordService $DailyRecordService)
    {
        try {
            // Mendapatkan daftar rekaman harian menggunakan DailyRecordService
            $Daily = $DailyRecordService->getAllDailyRecord($request);

            // Mengembalikan respons sukses dengan daftar rekaman harian menggunakan metode successResponsePagination()
            return $this->successResponsePagination($Daily);
        } catch (\Exception $e) {
            // Menangani kesalahan dengan mengembalikan respons error
            return $this->errorResponse($e);
        }
    }
}
