<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\UserService;
use App\Http\Traits\ApiResponder;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    use ApiResponder; // Menggunakan trait ApiResponder untuk respons API

    // Metode untuk menampilkan daftar pengguna
    public function index(Request $request, UserService $UserService)
    {
        try {
            // Memanggil metode getAllUsers() dari UserService untuk mendapatkan daftar pengguna
            $Users = $UserService->getAllUsers($request);

            // Mengembalikan respons sukses dengan daftar pengguna menggunakan metode successResponsePagination()
            return $this->successResponsePagination($Users);
        } catch (\Exception $e) {
            // Menangani kesalahan dengan mengembalikan respons error
            return $this->errorResponse($e);
        }
    }

    // Metode untuk menghapus pengguna
    public function destroy($id, UserService $UserService)
    {
        try {
            // Menghapus pengguna dengan ID yang diberikan menggunakan metode deleteUser() dari UserService
            $user = $UserService->deleteUser($id);

            // Mengembalikan respons sukses setelah pengguna dihapus
            return $this->successResponse($user);

        } catch (\Exception $e) {
            // Menangani kesalahan yang mungkin terjadi
            if ($e->getMessage() == 'User not found') {
                // Jika pengguna tidak ditemukan, kembalikan respons error dengan pesan khusus dan kode status yang sesuai
                return $this->errorResponse($e->getMessage(), $e->getCode());
            }
            // Jika kesalahan bukan karena pengguna tidak ditemukan, kembalikan respons error umum
            return $this->errorResponse($e->getMessage(), 400);
        }
    }
}
