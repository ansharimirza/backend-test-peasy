<?php

namespace App\Http\Traits;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait ApiResponder
{
    public function successResponsePagination ($data): JsonResponse
    {

        return response()->json([
            'status'  => 'success',
            'data'    => $data->items(),
            'pagination'  => [
                'current_page' => $data->currentPage(),
                'has_more' => $data->hasMorePages(),
                'total' =>  $data->total(),
            ]
        ], 200);
    }

    public function successResponse ($data): JsonResponse
    {
        return response()->json([
            'status'  => 'success',
            'data'    => $data
        ], 200);
    }

    public function errorResponse($message, $statusCode): JsonResponse
    {
        return response()->json([
            'status'  => 'failed',
            'message' => 'Something wrong with server - ' . $message
        ], $statusCode ?? 500);
    }
}
