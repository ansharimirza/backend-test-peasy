<?php

namespace App\Http\Services;

use App\Models\User;
use App\Models\DailyRecord;
use App\Events\UserDeleted;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserService
{
    // Metode untuk mendapatkan daftar pengguna
    public function getAllUsers($req)
    {
        // Mendapatkan query pencarian dari request
        $query = $req->query('search');
        // Mendapatkan nomor halaman dari request, jika tidak ada, default menjadi 1
        $page = $req->query('page') ?? 1;

        // Membuat query untuk mengambil data pengguna
        $usersQuery = User::query();

        // Jika ada query pencarian, tambahkan kriteria pencarian
        if ($query) {
            $usersQuery->where(function ($q) use ($query) {
                // Menggunakan ILIKE untuk pencarian case-insensitive
                $q->whereRaw('name->>\'first\' ILIKE ?', ["%{$query}%"])
                    ->orWhereRaw('name->>\'last\' ILIKE ?', ["%{$query}%"]);
            });
        }

        // Mengambil data pengguna dengan menggunakan paginate()
        $users = $usersQuery->paginate(10, ['*'], 'page', $page);

        return $users; // Mengembalikan data pengguna yang dipaginasi
    }

    // Metode untuk menghapus pengguna
    public function deleteUser($id)
    {
        try {
            // Mencari pengguna berdasarkan UUID, jika tidak ditemukan, lempar pengecualian
            $user = User::where('uuid', '=', $id)->firstOrFail();
        } catch (\Throwable $th) {
            // Menangani jika pengguna tidak ditemukan dengan melempar pengecualian dengan pesan khusus dan kode status
            throw new \Exception('User not found', 404);
        }

        // Kirim event bahwa pengguna telah dihapus
        event(new UserDeleted($user));

        return null; // Mengembalikan null karena pengguna telah dihapus
    }
}
