<?php
namespace App\Http\Services;


use App\Models\DailyRecord;


class DailyRecordService
{
    public function getAllDailyRecord($req)
    {
        // Mendapatkan nomor halaman dari request
        $page = $req->query('page') ?? 1;

        // Membuat query untuk mengambil data pengguna
        $dailyQuery = DailyRecord::query();

        // Mengambil data pengguna dengan menggunakan paginate()
        $daily = $dailyQuery->paginate(10, ['*'], 'page', $page);

        return $daily;
    }
}
