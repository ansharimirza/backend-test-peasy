<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\DailyRecord;
use Illuminate\Support\Facades\Redis;

class FetchRecords extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:records';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mengambil data jumlah pria dan wanita dari Redis dan menyimpannya ke dalam tabel DailyRecord';

    /**
     * Execute the console command.
     *
     * Menjalankan perintah konsol.
     */
    public function handle()
    {
        // Hitung total jumlah pria dan wanita dari Redis
        $maleCount = Redis::get('male_count');
        $femaleCount = Redis::get('female_count');

        // Simpan total jumlah pria dan wanita ke dalam tabel DailyRecord
        $dataDaily = DailyRecord::create([
            'male_count' => $maleCount,
            'female_count' => $femaleCount,
            'date' => now() // Menyimpan tanggal hari ini
        ]);
    }
}
