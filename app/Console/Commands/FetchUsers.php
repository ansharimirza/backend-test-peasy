<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Http;

class FetchUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mengambil pengguna dari API dan menyimpannya ke dalam database';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * Menjalankan perintah konsol.
     */
    public function handle()
    {
        // Mengambil data pengguna dari API eksternal
        $response = Http::get('https://randomuser.me/api/?results=20');
        $usersData = $response->json()['results'];

        // Mengambil jumlah pengguna laki-laki dan perempuan dari Redis, jika tidak ada, diinisialisasi menjadi 0
        $maleCount = Redis::get('male_count') ?? 0;
        $femaleCount = Redis::get('female_count') ?? 0;

        // Iterasi melalui data pengguna yang diterima dari API
        foreach ($usersData as $userData) {
            // Mengecek apakah pengguna sudah ada berdasarkan UUID
            $existingUser = User::where('uuid', $userData['login']['uuid'])->first();

            // Jika pengguna sudah ada, perbarui informasinya
            if ($existingUser) {
                $existingUser->update([
                    'name' => json_encode(['first' => $userData['name']['first'], 'last' => $userData['name']['last']]),
                    'gender' => $userData['gender'],
                    'location' => json_encode(['city' => $userData['location']['city'], 'state' => $userData['location']['state'], 'country' => $userData['location']['country']]),
                    'age' => $userData['dob']['age']
                ]);
            } else { // Jika pengguna belum ada, buat pengguna baru
                User::create([
                    'uuid' => $userData['login']['uuid'],
                    'name' => json_encode(['first' => $userData['name']['first'], 'last' => $userData['name']['last']]),
                    'gender' => $userData['gender'],
                    'location' => json_encode(['city' => $userData['location']['city'], 'state' => $userData['location']['state'], 'country' => $userData['location']['country']]),
                    'age' => $userData['dob']['age']
                ]);
            }

            // Hitung jumlah pengguna laki-laki dan perempuan
            if ($userData['gender'] === 'male') {
                $maleCount++;
            } elseif ($userData['gender'] === 'female') {
                $femaleCount++;
            }
        }

        // Simpan jumlah pengguna laki-laki dan perempuan ke dalam Redis
        Redis::set('male_count', $maleCount);
        Redis::set('female_count', $femaleCount);

        // Tampilkan pesan informasi bahwa pengguna telah diambil dan disimpan dengan sukses
        $this->info('Users fetched and stored successfully.');
    }
}
