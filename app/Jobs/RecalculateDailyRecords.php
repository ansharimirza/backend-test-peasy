<?php

namespace App\Jobs;

use Carbon\Carbon;
use App\Models\User;
use App\Models\DailyRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RecalculateDailyRecords implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userGender;

    /**
     * Create a new job instance.
     *
     * @param string $userGender
     */
    public function __construct($userGender)
    {
        $this->userGender = $userGender; // Menyimpan jenis kelamin pengguna
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Mengambil rekaman harian untuk hari ini
        $dailyRecord = DailyRecord::whereDate('date', now()->toDateString())->first();

        // Jika tidak ada rekaman harian untuk hari ini, cari rekaman hari sebelumnya
        if (!$dailyRecord) {
            $yesterday = Carbon::yesterday();
            $dailyRecord = DailyRecord::whereDate('date', $yesterday->toDateString())->first();
        }

        // Jika ada rekaman harian yang ditemukan
        if ($dailyRecord) {
            // Mengurangi jumlah pengguna sesuai dengan jenis kelaminnya dan mengupdate rata-rata usia
            if ($this->userGender === 'male') {
                $dailyRecord->decrement('male_count');
                $dailyRecord->male_avg_age = User::where('gender', 'male')->avg('age');

                // Mengurangi jumlah pengguna laki-laki dalam Redis
                Redis::decr('male_count');
            } else {
                $dailyRecord->decrement('female_count');
                $dailyRecord->female_avg_age = User::where('gender', 'female')->avg('age');

                // Mengurangi jumlah pengguna perempuan dalam Redis
                Redis::decr('female_count');
            }

            // Menyimpan perubahan pada rekaman harian
            $dailyRecord->save();
        }
    }
}
