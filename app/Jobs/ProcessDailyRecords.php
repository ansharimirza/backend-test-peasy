<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\DailyRecord;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class ProcessDailyRecords implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Handle the job.
     *
     * Menghandle pekerjaan.
     */
    public function handle()
    {
        // Hitung total jumlah pria dan wanita dari Redis
        $maleCount = Redis::get('male_count');
        $femaleCount = Redis::get('female_count');

        // Simpan total jumlah pria dan wanita ke dalam tabel DailyRecord
        $dataDaily = DailyRecord::create([
            'male_count' => $maleCount,
            'female_count' => $femaleCount,
            'date' => now() // Menyimpan tanggal hari ini
        ]);
    }
}
