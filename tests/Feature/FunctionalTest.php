<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

class FunctionalTest extends TestCase
{
    use DatabaseTransactions;

    public function test_fetching_user_records()
    {
        Http::fake([
            'https://randomuser.me/api/*' => Http::response([
                'results' => [
                    [
                        'login' => ['uuid' => 'd1242e15-4411-42c2-aaef-1224694ed1d8'],
                        'gender' => 'male',
                        'name' => ['first' => 'John', 'last' => 'Doe'],
                        'location' => ['city' => 'New York', 'state' => 'NY', 'country' => 'USA'],
                        'dob' => ['age' => 30]
                    ],
                    // Add more sample user data here if needed
                ]
            ], 200)
        ]);

        $this->artisan('fetch:users')
             ->expectsOutput('20 user records fetched and stored successfully.')
             ->assertExitCode(0);

        // Assert database contains fetched records
        $this->assertDatabaseCount('users', 1);

        $user = \App\Models\User::where('uuid', 'd1242e15-4411-42c2-aaef-1224694ed1d8')->first();

        $this->assertEquals('male', $user->gender);
        $this->assertEquals('John', $user->name['first']);
        $this->assertEquals('Doe', $user->name['last']);
        $this->assertEquals('New York', $user->location['city']);
        $this->assertEquals(30, $user->age);

        // Assert Redis contains gender counts
        $this->assertEquals(1, Redis::hget('gender_count', 'male'));
        $this->assertEquals(0, Redis::hget('gender_count', 'female'));
    }
}
